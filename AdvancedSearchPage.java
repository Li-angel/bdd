package pages;

import org.jsoup.Connection;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AdvancedSearchPage extends BasePage {

    public AdvancedSearchPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//input[@id='_nkw']")
    private WebElement enterYourSearchItemField;

    @FindBy(xpath = "//input[@name='_udlo']")
    private WebElement enterMinPrice;

    @FindBy(xpath = "//input[@name='_udhi']")
    private WebElement enterMaxPrice;

    @FindBy(xpath = "//label[contains(@for,'New')]")
    private WebElement onlyNewItems;

    @FindBy(xpath = "//label[contains(@for,'BIN')]")
    private WebElement buyItNow;

    @FindBy(xpath = "//select[@name='_ipg']//option[@value='25']")
    private WebElement resultsPerPage;

    @FindBy(xpath = "//span[@class='adv-s mb']//button[contains(@class,'btn-prim')]")
    private WebElement searchButton;

    @FindBy(xpath = "//span[@class='rcnt']")
    private WebElement calculatedNumberOfElementsByUserRequest;

    public void enterTextToSearchItemField(final String searchText) {
        enterYourSearchItemField.clear();
        enterYourSearchItemField.sendKeys(searchText);
    }

    public void enterMinPriceToField(final String searchText) {
        enterMinPrice.clear();
        enterMinPrice.sendKeys(searchText);
    }

    public void enterMaxPriceToField(final String searchText) {
        enterMaxPrice.clear();
        enterMaxPrice.sendKeys(searchText);
    }

    public void setSearchOnlyNewItems(){
        onlyNewItems.click();
    }
    public void setBuyItNow(){
        buyItNow.click();
    }

    public void clickOnSearchButton(){
        searchButton.click();
    }

    public WebElement getCalculatedNumberOfElementsByUserRequestCount(){
        return calculatedNumberOfElementsByUserRequest;
    }

    public String getAmountOfProducts() {
        return calculatedNumberOfElementsByUserRequest.getText();
    }
    public void clickOnQuantityOfResultsPerPage(){
        resultsPerPage.click();
    }
}
