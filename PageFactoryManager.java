package manager;

import org.openqa.selenium.WebDriver;
import pages.*;

public class PageFactoryManager {

    WebDriver driver;

    public PageFactoryManager(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage getHomePage() {
        return new HomePage(driver);
    }

    public KoreanCosmeticsPage getKoreanCosmeticsPagePage() {
        return new KoreanCosmeticsPage(driver);
    }


    public FordF150Page getFordF150Page() {
        return new FordF150Page(driver);
    }

    public AdvancedSearchPage getAdvancedSearchPage(){return new AdvancedSearchPage(driver);}

    public ChargerPage getChargerPage(){return new ChargerPage(driver);}

    public SonyPage getSonyPage(){return new SonyPage(driver);}

    public SearchResultsPage getSearchResultPage(){return new SearchResultsPage(driver);}

    public InvestorsPage getInvestorsPage(){return new InvestorsPage(driver);}

}
