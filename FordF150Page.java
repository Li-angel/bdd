package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FordF150Page extends BasePage{
    public FordF150Page(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//li[contains(@class,'--value')]//a[contains(@href,'4WD')]")
    private WebElement only4WDFilter;

    @FindBy(xpath = "//li[contains(@class,'--value')]//a[contains(@href,'2021')]")
    private WebElement only2021Filter;

    @FindBy(xpath = "//li[contains(@class,'--value')]//a[contains(@href,'White')]")
    private WebElement onlyWhiteColorFilter;

    @FindBy(xpath = "//div[contains(@class,'s-item__info')]//a[contains(@href,'item46d0a1e2e5:g:kJAAAOS')]")
    private WebElement orderCar;

    @FindBy(xpath = "//div[contains(@id,'vi-atl-lnk')]")
    private WebElement addToWatchList;

    @FindBy(xpath = "//div[@class='id-first']")
    private WebElement idFirstPage;

    public void clickOnly4WDFilter(){only4WDFilter.click();}

    public void clickOnly2021Filter(){only2021Filter.click();}

    public void clickOnlyWhiteColorFilter(){onlyWhiteColorFilter.click();}

    public void clickOnOrderedCar(){orderCar.click();}

    public void clickAddToWatchList(){addToWatchList.click();}

    public boolean isIdFirstPageVisible(){idFirstPage.isDisplayed();
        return false;
    }
}
