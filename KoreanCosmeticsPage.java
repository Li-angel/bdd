package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class KoreanCosmeticsPage extends BasePage{

    public KoreanCosmeticsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[contains(@class,'b-event')]//a[contains(@href,'dr')]")
    private WebElement drJartCosmeticsButton;

    @FindBy(xpath = "//div[contains(@class,'single')]//a[contains(@href,'BIN')]")
    private WebElement buyItNowFilter;

    @FindBy(xpath = "//div[contains(@class,'single')]//a[contains(@href,'BIN=1&rt=nc&LH_PrefLoc=6')]")
    private WebElement onlyAsiaLocationFilter;

    @FindBy(xpath = "//div[contains(@class,'s-item__info clearfix')]//a[contains(@href,'itm/28370563')]")
    private WebElement orderedItem;


    @FindBy(xpath = "//div[contains(@class,'s-item__info ')]//a[contains(@href,'1d057c10')]")
    private WebElement item2;

    @FindBy(xpath = "//div[contains(@class,'s-item__info ')]//a[contains(@href,'3f86f')]")
    private WebElement item3;

    public void clickDrJartCosmticsButton() {
        drJartCosmeticsButton.click();
    }

    public void clickBuyItNowFilter(){buyItNowFilter.click();}

    public void clickOnlyAsiaLocationFilter(){onlyAsiaLocationFilter.click();}

    public void clickOnOrderedItem(){orderedItem.click();}

    public void clickOnItem2(){item2.click();}

    public void clickOnItem3(){item3.click();}




}
