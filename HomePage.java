package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage{

    public HomePage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//footer[contains(@id, 'footer')]")
    private WebElement footer;

    @FindBy(xpath = "//header[@id='gh']")
    private WebElement header;

    @FindBy(xpath = "//input[contains(@class, '-input')]")
    private WebElement searchField;

    @FindBy(xpath = "//input[@id='gh-btn']")
    private WebElement searchButton;

    @FindBy(xpath = "//li[contains(@class, 'popular')]//a[contains(@href,'Beauty')]")
    private WebElement popularKoreanCosmeticsButton;

    @FindBy(xpath = "//a[contains(@href, 'twitter')]")
    private WebElement twitterButton;

    @FindBy(xpath = "//a[contains(@id,'gh-as' )]")
    private WebElement advancedSearch;

    @FindBy(xpath = "//a[contains(@href, 'cart.payments.ebay.com/')]")
    private WebElement cartIcon;

    @FindBy(xpath = "//a[contains(@id,'isCartBtn')]")
    private WebElement addToCartButton;

    @FindBy(xpath = "//a[contains(@class,'watchlist')]")
    private WebElement watchList;

    @FindBy(xpath = "//div[@id='vi-atl-lnk']")
    private WebElement addToWatchList;

    @FindBy(xpath = "//a[contains(@href,'reg.ebay')]")
    private WebElement registerButton;

    @FindBy(xpath = "//input[@id='firstname']")
    private WebElement firstnameField;

    @FindBy(xpath = "//input[@id='lastname']")
    private WebElement lastnameField;

    @FindBy(xpath = "//input[@id='Email']")
    private WebElement emailField;

    @FindBy(xpath = "//input[@id='password']")
    private WebElement passwordField;

    @FindBy(xpath = "//a[contains(@href,'Sony')]")
    private WebElement sonyCategory;

    @FindBy(xpath = "//a[@href='https://www.ebay.com/']")
    private WebElement returnToHomeEbayPage;

    @FindBy(xpath = "//form[@name='create-personal-account-with-email']")
    private WebElement registerForm;

    @FindBy(xpath = "//a[contains(@id,'binBtn')]")
    private WebElement buyItNowButton;

    @FindBy(xpath = "//div[@id='streamline-bin-layer']")
    private WebElement signInOrCheckOutForm;

    @FindBy(xpath = "//div[@id='wrapper']")
    private WebElement captchaForm;

    @FindBy(xpath = "//div[contains(@class,'top')]//h1[contains(@class,'main-title')]")
    private WebElement checkQuantityOfOrderedItems;

    public boolean isCaptchaFormVisible(){captchaForm.isDisplayed();
        return false;
    }

    public void openHomePage(String url) {
        driver.get(url);
    }

    public void isHeaderVisible() {
        header.isDisplayed();
    }

    public void isFooterVisible() {
        footer.isDisplayed();
    }

    public void isCartIconVisible() {
        cartIcon.isDisplayed();
    }

    public void isSearchFieldVisible() {
        searchField.isDisplayed();
    }

    public void isWatchlistVisible(){watchList.isDisplayed();}

    public WebElement getRegisterForm() {
        return registerForm;
    }

    public void isAdvancedSearchVisible(){advancedSearch.isDisplayed();}

    public void isTwitterVisible(){twitterButton.isDisplayed();}

    public void isRegisterButtonVisible(){registerButton.isDisplayed();}

    public boolean isFirstnameFieldVisible(){firstnameField.isDisplayed();
        return false;
    }

    public boolean isLastnameFieldVisible(){lastnameField.isDisplayed();
        return false;
    }

    public boolean isEmailFieldVisible(){emailField.isDisplayed();
        return false;
    }

    public boolean isSignInOrCheckOutFormVisible(){signInOrCheckOutForm.isDisplayed();
    return false;
    }

    public boolean isPasswordFieldVisible(){passwordField.isDisplayed();
        return false;
    }
    public WebElement getQuantityOfItemsInCartCount() {
        return checkQuantityOfOrderedItems;
    }

    public String getAmountOfProductsInCart() {
        return checkQuantityOfOrderedItems.getText();
    }

    public void clickCartButton() {
        cartIcon.click();
    }
    public void clickSonyCategory(){sonyCategory.click();}

    public void clickToReturnEbayHomePage(){returnToHomeEbayPage.click();}

    public void clickBuyItNowButton(){buyItNowButton.click();}

    public void clickAddToCartButton(){addToCartButton.click();}

    public void clickToRegisterButton(){registerButton.click();}

    public void clickPopularKoreanCosmeticsButton(){popularKoreanCosmeticsButton.click();}

    public void enterTextToSearchField(final String searchText) {
        searchField.clear();
        searchField.sendKeys(searchText);
    }

    public void clickSearchButton() {
        searchButton.click();
    }

    public void clickTwitterButton(){twitterButton.click();}

    public void clickAdvancedSearch(){advancedSearch.click();}

    public void clickWatchListButton(){watchList.click();}

    public void clickAddToWatchList(){addToWatchList.click();}




}
