package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SonyPage extends BasePage{

    public SonyPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//li[contains(@class,'fake')]//a[contains(@href,'BIN=1')]")
    private WebElement buyItNowSmall;

    public void clickToBuyItNowSmall(){buyItNowSmall.click();}


    @FindBy(xpath = "//div[@class='s-item__info clearfix']//a[contains(@href,'item4b9f0f')]")
    private WebElement elementForBuying;

    public void clickElementForBuying(){elementForBuying.click();}

    @FindBy(xpath = "//div[contains(@class, 'u-cb')]//a[contains(@href, 'CART')]")
    private WebElement addToCartForSony;

    public void clickAddToCartForSony(){addToCartForSony.click();}

    @FindBy(xpath = "//div[contains(@id, '106')]//a[contains(@href, 'pageci=10d')]")
    private WebElement viewInCartButton;

    public boolean isViewCartButtonVisible(){viewInCartButton.isDisplayed();
        return false;
    }




}
