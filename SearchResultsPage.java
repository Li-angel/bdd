package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchResultsPage extends BasePage{

    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[contains(@class,'s-item__info clearfix')]//a[contains(@href,'/itm/2024')]")
    private WebElement bigTeddyBear;

    public void clickOnBigTeddyBear(){bigTeddyBear.click();}

    @FindBy(xpath = "//div[contains(@class,'s-item__info clearfix')]//a[contains(@href,'/itm/1422')]")
    private WebElement tiara;

    public void clickOnTiara(){tiara.click();}

    @FindBy(xpath = "//button[contains(@aria-label,'Remove - 47')]")
    private WebElement removeTeddyItem;

    public void removeOneItemFromCart(){removeTeddyItem.click();}
}
