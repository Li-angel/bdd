package stepdefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.*;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static org.junit.Assert.*;

public class DefinitionSteps {

        private static final long DEFAULT_TIMEOUT = 90;
        WebDriver driver;
        HomePage homePage;
        KoreanCosmeticsPage koreanCosmeticsPage;
        SearchResultsPage searchResultsPage;
        AdvancedSearchPage advancedSearchPage;
        FordF150Page fordF150Page;
        PageFactoryManager pageFactoryManager;
        ChargerPage chargerPage;
        SonyPage sonyPage;
        InvestorsPage investorsPage;


    @Before
        public void testsSetUp() {
            chromedriver().setup();
            driver = new ChromeDriver();
            driver.manage().window().maximize();
            pageFactoryManager = new PageFactoryManager(driver);
        }
    @And("User opens {string} page")
    public void openPage(final String url) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(url);
    }

    @And("User checks header visibility")
    public void checkHeaderVisibility() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        homePage.isHeaderVisible();
    }

    @And("User checks search field visibility")
    public void checkSearchVisibility() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.isSearchFieldVisible();
    }

    @And("User checks footer visibility")
    public void checkFooterVisibility() {
        homePage.isFooterVisible();
    }

    @And("User checks cart visibility")
    public void checkCartVisibility() {
        homePage.isCartIconVisible();
    }

    @And("User checks watchList visibility")
    public void checkWatchListVisibility(){homePage.isWatchlistVisible();}

    @And("User checks advanced search visibility")
    public void checkAdvancedSearchVisibility(){homePage.isAdvancedSearchVisible();}

    @And("User checks twitter button visibility")
    public void checkTwitterButtonVisibility(){homePage.isTwitterVisible();}

    @And("User checks register button visibility")
    public void checkRegisterButtonVisibility() {
        homePage.isRegisterButtonVisible();
    }

    @When("User clicks 'Register' button")
    public void clickSignInButton() {
        homePage.clickToRegisterButton();
    }

    @Then("User checks firstname, lastname, email and password fields visibility on register popup")
    public void checkEmailVisibility() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getRegisterForm());
        assertTrue(homePage.isEmailFieldVisible());
        assertTrue(homePage.isPasswordFieldVisible());
        assertTrue(homePage.isFirstnameFieldVisible());
        assertTrue(homePage.isLastnameFieldVisible());
    }

    @And("User returns to ebay home page")
    public void closeSignInPopup() {
        homePage.clickToReturnEbayHomePage();
    }

    @And("User makes search by keyword {string}")
    public void enterKeywordToSearchField(final String keyword) {
        homePage.enterTextToSearchField(keyword);
    }

    @And("User clicks search button")
    public void clickSearchButton() {homePage.clickSearchButton();}

    @And("User clicks on filters button")
    public void clickFiltersForF150() {
        fordF150Page = pageFactoryManager.getFordF150Page();
        fordF150Page.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        fordF150Page.waitForAjaxToCompletePdp(DEFAULT_TIMEOUT);
        fordF150Page.clickOnly4WDFilter();
        fordF150Page.clickOnly2021Filter();
        fordF150Page.clickOnlyWhiteColorFilter();
    }

    @And("User orders a car")
    public void clickOnOrderedCar(){
        fordF150Page.clickOnOrderedCar();
    }

    @And("User adds it to watchlist")
    public void clickAddToWatchList(){
        fordF150Page.clickAddToWatchList();
    }

    @Then("User must see sign in form")
    public void checkSignInVisibility(){
        assertFalse(homePage.isCaptchaFormVisible());
    }

    @And("User clicks on popular korean cosmetics button")
    public void clickOnPopularKoreanCosmetics(){homePage.clickPopularKoreanCosmeticsButton();}

    @And("User clicks on popular category dr:Jart")
    public void clickKoreanCosmeticsDrJart() {
        koreanCosmeticsPage = pageFactoryManager.getKoreanCosmeticsPagePage();
        koreanCosmeticsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        koreanCosmeticsPage.waitForAjaxToCompletePdp(DEFAULT_TIMEOUT);
        koreanCosmeticsPage.clickDrJartCosmticsButton();
    }

    @And("User filters results with 'only Asia'")
    public void clickOnlyAsia(){koreanCosmeticsPage.clickOnlyAsiaLocationFilter();}

    @And("User filters results with 'buy it now'")
    public void clickBuyItNowFilter() {koreanCosmeticsPage.clickBuyItNowFilter();}

    @And("User clicks on ordered item")
    public void clickOnOrderedItem(){koreanCosmeticsPage.clickOnOrderedItem();}

    @And("User clicks add to cart")
    public void clickOnAddToCart(){homePage.clickAddToCartButton();}

    @And("User checks that amount of products in cart are {string}")
    public void checkAmountOfProductsInWishList(final String expectedAmount) {
        koreanCosmeticsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getQuantityOfItemsInCartCount());
        assertEquals(homePage.getAmountOfProductsInCart(), expectedAmount);
    }

    @And("User clicks on advanced search button")
    public void clickOnAdvancedSearch() {
        advancedSearchPage = pageFactoryManager.getAdvancedSearchPage();
        homePage.clickAdvancedSearch();
        advancedSearchPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        advancedSearchPage.waitForAjaxToCompletePdp(DEFAULT_TIMEOUT);
    }

    @And("User enters text {string} to search field")
    public void enterTextToSearchField(final String keyword){
            advancedSearchPage.enterTextToSearchItemField(keyword);}

    @And("User enters min {string} price")
    public void enterMinPrice(final String minimal){
        advancedSearchPage.enterMinPriceToField(minimal);
    }

    @And("User enters max {string} price")
    public void enterMaxPrice(final String maximal){
        advancedSearchPage.enterMaxPriceToField(maximal);
    }

    @And("User clicks to filer 'only new items'")
    public void filterOnlyNewItems(){advancedSearchPage.setSearchOnlyNewItems();}

    @And("User clicks to filter 'buy iy now'")
    public void filterBuyItNow(){advancedSearchPage.setBuyItNow();}

    @And("User clicks on search button")
    public void clickOnSearchButton(){advancedSearchPage.clickOnSearchButton();}

    @And("User clicks on quantity of results per page")
    public void clickOnQuantityOfResultsPerPage(){advancedSearchPage.clickOnQuantityOfResultsPerPage();}

    @And("User checks the correct that amount of products are {string}")
    public void checkAmountOfProductsOnRequest(final String expected) {
        advancedSearchPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        advancedSearchPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, advancedSearchPage.getCalculatedNumberOfElementsByUserRequestCount());
        assertEquals(advancedSearchPage.getAmountOfProducts(), expected);
    }

    @And("User clicks on item for buying")
    public void clickOnItem() {
        chargerPage = pageFactoryManager.getChargerPage();
        chargerPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        chargerPage.waitForAjaxToCompletePdp(DEFAULT_TIMEOUT);
        chargerPage.clickOnChargerButton();}

    @And("User orders color")
    public void orderColor(){
        chargerPage.setBlackColor();}

    @And("User sets the number {string} of wanted item")
    public void enterTheNumberOfWantedItem(final String number) {
        chargerPage.setNumberOfItems(number);
    }

    @And("User clicks 'buy now'")
    public void buyNowButton(){homePage.clickBuyItNowButton();}

    @And("User checks visibility of sign in form after clicking buy it now")
    public void visibilityOfSignInForm(){
        assertFalse(homePage.isSignInOrCheckOutFormVisible());
    }

    @And("User clicks twitter button")
    public void clickOnTwitterButton(){homePage.clickTwitterButton();}

    @And("User checks that current url contains {string} word")
    public void checkCurrentUrl(final String word) {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertTrue(driver.getCurrentUrl().contains(word));
    }

    @And("User clicks to sony category")
    public void clickToSonyCategory(){
        sonyPage = pageFactoryManager.getSonyPage();
        sonyPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        sonyPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        homePage.clickSonyCategory();}

    @And("User clicks to buy it now filter")
    public void clickToBuyItNow(){
        sonyPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        sonyPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        sonyPage.clickToBuyItNowSmall();}

    @And("User clicks on ordered sony item")
    public void checkTheQuantityOfElements() {
        sonyPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        sonyPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        sonyPage.clickElementForBuying();
    }

    @And("User clicks on add to watchlist")
    public void addToWatchlist(){
        sonyPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        sonyPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        homePage.clickAddToWatchList();
    }

    @And("User checks visibility of captcha form")
    public void isCaptchaVisible(){
        assertTrue(homePage.isCaptchaFormVisible());
    }

    @And("User order item2")
    public void orderItem2(){koreanCosmeticsPage.clickOnItem2();}

    @And("User order item3")
    public void orderItem3(){koreanCosmeticsPage.clickOnItem3();}

    @And("User clicks on item big teddy bear")
    public void addsTeddyBear(){
        searchResultsPage = pageFactoryManager.getSearchResultPage();
        searchResultsPage.clickOnBigTeddyBear();}

    @And("User clicks on item tiara")
    public void addsTiara(){searchResultsPage.clickOnTiara();}

    @And("User removes teddy bear item")
    public void removeTeddyItem(){searchResultsPage.removeOneItemFromCart();}

    @And("User clicks on investors button")
    public void clickInvestorsButton(){
        investorsPage = pageFactoryManager.getInvestorsPage();
        investorsPage.clickInvestorsButton();
        investorsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        investorsPage.waitForAjaxToCompletePdp(DEFAULT_TIMEOUT);}


    @And("User checks visibility of investors page")
    public void checkVisibilityOfGrid(){
        investorsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        investorsPage.waitForAjaxToCompletePdp(DEFAULT_TIMEOUT);
        assertTrue(investorsPage.isGridVisible());

    }
    @And("User clicks on contact button")
        public void clickOnContactButton() {
        investorsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        investorsPage.waitForAjaxToCompletePdp(DEFAULT_TIMEOUT);
        investorsPage.clickContactButton();}

    @And("User checks the ability to see contact Form")
            public void checkVisibilityOfContactForm(){
        assertTrue(investorsPage.isContactFormVisible());
    }

    @And("User adds to cart on Sony page")
    public void clickOnAddToCartForSony(){sonyPage.clickAddToCartForSony();}

    @And("User checks the ability to see view cart button")
    public void checkVisibilityOfViewCartButton(){
        assertFalse(sonyPage.isViewCartButtonVisible());
    }




}

