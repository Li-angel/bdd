package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class InvestorsPage extends BasePage{
    public InvestorsPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//a[contains(@href,'inves')]")
    private WebElement investorsButton;

    public void clickInvestorsButton(){investorsButton.click();}

    @FindBy(xpath = "//div[@class='grid']")
    private WebElement grid;



    public boolean isGridVisible(){grid.isDisplayed();
        return true;
    }

    @FindBy(xpath = "//a[@href='/contact-us-and-resources/contact-us/default.aspx']")
    private WebElement contactButton;

    public void clickContactButton(){contactButton.click();}

    @FindBy(xpath = "//div[@role='form']//div[contains(@class, '--inner')]")
    private WebElement contactForm;

    public boolean isContactFormVisible(){contactForm.isDisplayed();
        return false;
    }
}
