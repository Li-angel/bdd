package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ChargerPage extends BasePage{

    public ChargerPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//div[@class='s-item__info clearfix']//a[contains(@href,'item1d0d')]")
    private WebElement chargerButton;

    @FindBy(xpath = "//select[@id='msku-sel-1']//option[@id='msku-opt-4']")
    private WebElement blackColor;

    @FindBy(xpath = "//input[@class='qtyInput']")
    private  WebElement inputNumberOfItems;

    public void clickOnChargerButton(){chargerButton.click();}

    public void setBlackColor(){blackColor.click();}

    public void setNumberOfItems(final String searchText) {
        inputNumberOfItems.clear();
        inputNumberOfItems.sendKeys(searchText);
    }

}
